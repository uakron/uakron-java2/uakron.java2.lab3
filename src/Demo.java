import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Demo
{
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) throws EmptyStringException, InvalidNumberException, FileNotFoundException, ClassNotFoundException, IOException
	{
		var payroll = new Payroll(getName(), getIdNum());
		
		payroll.setHours(getHours());
		payroll.setRate(getRate());
		
		input.close();
		
		payroll.writeToFile("payroll.txt");
		payroll.readFromFile("payroll.txt");
	}
	
	private static String getName() throws EmptyStringException
	{
		System.out.println("Please enter the employee's name:");
		return input.nextLine();
	}
	
	private static int getIdNum() throws InvalidNumberException
	{
		System.out.println("Enter the employee's ID number:");
		return Integer.parseInt(input.nextLine());
	}
	
	private static int getHours() throws InvalidNumberException
	{
		System.out.println("Please enter the hours worked:");
		return Integer.parseInt(input.nextLine());
	}
	
	private static double getRate()
	{
		System.out.println("Please enter the employee's hourly rate:");
		return input.nextDouble();
	}
}
