import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.Test;

class PayrollTests {

	@Test
	void payrollTests() throws EmptyStringException, InvalidNumberException, FileNotFoundException, IOException {
		var payroll = new Payroll("Bailey Parrish", 10020180);
		
		assertEquals("Bailey Parrish", payroll.getName());
		assertEquals(10020180, payroll.getIdNumber());
		
		payroll.setName("B. Parrish");
		payroll.setIdNumber(64966591);
		
		assertEquals("B. Parrish", payroll.getName());
		assertEquals(64966591, payroll.getIdNumber());
		
		payroll.setHours(40);
		payroll.setRate(19.00);
		
		assertEquals(40, payroll.getHours());
		assertEquals(19.00, payroll.getRate());
		assertEquals(760, payroll.getGrossPay());
		
		assertThrows(EmptyStringException.class, () -> payroll.setName(""));
		assertThrows(InvalidNumberException.class, () -> payroll.setIdNumber(0));
		assertThrows(InvalidNumberException.class, () -> payroll.setHours(-5));
		assertThrows(InvalidNumberException.class, () -> payroll.setRate(100.00));
		
		assertThrows(FileNotFoundException.class, () -> payroll.writeToFile(""));
		assertThrows(FileNotFoundException.class, () -> payroll.readFromFile(""));
	}
}
