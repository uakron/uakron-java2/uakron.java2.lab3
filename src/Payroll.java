import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Payroll implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String name;
	private int idNumber;
	private double rate;
	private int hours;
	
	public Payroll(String name, int idNumber)
	{
		this.name = name;
		this.idNumber = idNumber;
	}
	
	public void setName(String newName) throws EmptyStringException
	{
		if (newName == "")
			throw new EmptyStringException("Name must contain a value.");
		this.name = newName;
	}
	
	public void setIdNumber(int newIdNumber) throws InvalidNumberException
	{
		if (newIdNumber <= 0)
			throw new InvalidNumberException("Id number must be greater than 0.");
		this.idNumber = newIdNumber;
	}
	
	public void setRate(double newRate) throws InvalidNumberException
	{
		if (newRate < 0 || newRate > 25)
			throw new InvalidNumberException("Rate must be between 0 and 25.");
		this.rate = newRate;
	}
	
	
	public void setHours(int newHours) throws InvalidNumberException
	{
		if (newHours < 0 || newHours > 84)
			throw new InvalidNumberException("Hours must be between 0 and 84.");
		this.hours = newHours;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public int getIdNumber()
	{
		return this.idNumber;
	}
	
	public double getRate()
	{
		return this.rate;
	}
	
	public int getHours()
	{
		return this.hours;
	}
	
	public double getGrossPay()
	{
		return this.hours * this.rate;
	}
	
	public void writeToFile(String fileName) throws IOException
	{
		if (fileName.length() == 0)
			throw new FileNotFoundException("Invalid file name.");
		
		FileOutputStream fstream = new FileOutputStream(new File(fileName));
		ObjectOutputStream ostream = new ObjectOutputStream(fstream);
			
		ostream.writeObject(this);
		ostream.close();
	}
	
	public void readFromFile(String fileName) throws IOException, ClassNotFoundException
	{
		if (fileName.length() == 0)
			throw new FileNotFoundException("Invalid file name.");
		
		FileInputStream fileInput = new FileInputStream(new File(fileName));
		ObjectInputStream objectInput = new ObjectInputStream(fileInput);
		
		Payroll payroll = (Payroll) objectInput.readObject();
		
		System.out.println("Name: " + payroll.getName());
		System.out.println("Id: " + payroll.getIdNumber());			
		System.out.println("Hours: " + payroll.getHours());
		System.out.println("Rate: " + payroll.getRate());
		System.out.println("Gross pay: " + payroll.getGrossPay());
		
		fileInput.close();
		objectInput.close();
	}
}
